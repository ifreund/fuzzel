#compdef fuzzel

_arguments \
    -s \
    '(-v --version)'{-v,--version}'[show the version number and quit]' \
    '(-h --help)'{-h,--help}'[show help message and quit]' \
    '(-o --output)'{-o,--output}'[output (monitor) to display on (none)]:output:->outputs' \
    '(-f --font)'{-f,--font}'[font name and style in fontconfig format (monospace)]:font:->fonts' \
    '(-i --icon-theme)'{-i,--icon-theme}'[icon theme name (hicolor)]:theme:->icon_theme' \
    '(-T --terminal)'{-T,--terminal}'[terminal command, with arguments]:terminal:_command_names -e' \
    '(-l,--lines)'{-l,--lines}'[maximum number of matches to display (15)]:()' \
    '(-w,--width)'{-w,--width}'[window width, in characters (30)]:()' \
    '(-b --background)'{-b,--background}'[background color (fdf6e3dd)]:background:()' \
    '(-t --text-color)'{-t,--text-color}'[text color (657b83ff)]:text-color:()' \
    '(-m --match-color)'{-m,--match-color}'[color of matched substring (cb4b16ff)]:match-color:()' \
    '(-s --selection-color)'{-s,--selection-color}'[background color of selected item (eee8d5ff)]:selection-color:()' \
    '(-B --border-width)'{-B,--border-width}'[width of border, in pixels (1)]:border-width:()' \
    '(-r --border-radius)'{-r,--border-radius}'[amount of corner "roundness" (10)]:border-radius:()' \
    '(-C --border-color)'{-C,--border-color}'[border color (002b36ff)]:border-color:()' \
    '(-d --dmenu)'{-d,--dmenu}'[dmenu compatibility mode; list entries are read from stdin]' \
    '(-R --no-run-if-empty)'{-R,--no-run-if-empty}'[exit immediately without showing the UI if stdin is empty (dmenu mode only)]'

case "${state}" in
    fonts)
        if command -v fc-list > /dev/null; then
            _values 'font families' $(fc-list : family | tr -d ' ')
        fi
        ;;

    icon_theme)
        _values 'icon themes' $(cd /usr/share/icons; echo *)
        ;;

    outputs)
        if command -v swaymsg > /dev/null; then
            _values 'outputs' $(swaymsg -t get_outputs --raw|grep name|cut -d '"' -f 4)
        fi
        ;;
esac
